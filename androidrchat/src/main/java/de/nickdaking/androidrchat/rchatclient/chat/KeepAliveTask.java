package de.nickdaking.androidrchat.rchatclient.chat;

import java.util.TimerTask;

/**
 * Created by Nickerchen on 15.02.14.
 */
public class KeepAliveTask extends TimerTask {
    private final Client client;

    public KeepAliveTask(Client client) {
        this.client = client;
    }

    @Override
    public void run() {
        if (client.isConnected()) {
            client.send("ping");
        }
    }
}
