package de.nickdaking.androidrchat.rchatclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import de.nickdaking.androidrchat.rchatclient.R;
import de.nickdaking.androidrchat.rchatclient.model.NavDrawerGroup;
import de.nickdaking.androidrchat.rchatclient.model.NavDrawerItem;


public class NavDrawerListAdapter extends BaseExpandableListAdapter {
     
    private Context context;
    private ArrayList<NavDrawerGroup> navDrawerGroups;
     
    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerGroup> navDrawerGroups){
        this.context = context;
        this.navDrawerGroups = navDrawerGroups;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return navDrawerGroups.get(groupPosition).getNavDrawerItem(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        NavDrawerGroup groupItem = (NavDrawerGroup) getGroup(groupPosition);

        if (groupItem.getChildCount() == 1) {
            ((ExpandableListView) parent).collapseGroup(groupPosition);
            return getChildView(groupPosition, 0, true, convertView, parent);
        }

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

        if (imgIcon != null) {
            imgIcon.setImageResource(groupItem.getIcon());
        }
        txtTitle.setText(groupItem.getTitle());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        NavDrawerItem childItem = (NavDrawerItem) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.drawer_list_item, null);
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        TextView txtCount = (TextView) convertView.findViewById(R.id.counter);

        imgIcon.setImageResource(childItem.getIcon());
        txtTitle.setText(childItem.getTitle());

        if(childItem.getCounterVisibility()) {
            if (childItem.getCount() != 0) {
                txtCount.setVisibility(View.VISIBLE);
                txtCount.setText(String.valueOf(childItem.getCount()));
            }
        } else {
            txtCount.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return navDrawerGroups.get(groupPosition).getChildCount();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return navDrawerGroups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return navDrawerGroups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void updateResults() {
        notifyDataSetChanged();
    }
 
}