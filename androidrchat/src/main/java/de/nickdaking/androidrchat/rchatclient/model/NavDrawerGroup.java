package de.nickdaking.androidrchat.rchatclient.model;

import java.util.ArrayList;

/**
 * Created by Nickerchen on 23.02.14.
 */
public class NavDrawerGroup {
    private String title;
    private int icon;
    private ArrayList<NavDrawerItem> navDrawerItems;

    public NavDrawerGroup(String title, int icon, ArrayList<NavDrawerItem> navDrawerItems) {
        this.title = title;
        this.icon = icon;
        if (navDrawerItems == null) {
            this.navDrawerItems = new ArrayList<NavDrawerItem>();
        }
    }

    public NavDrawerGroup(NavDrawerItem item) {
        this.navDrawerItems = new ArrayList<NavDrawerItem>();
        navDrawerItems.add(item);
        this.title = item.getTitle();
        this.icon = item.getIcon();
    }

    public void addNavDrawerItem(NavDrawerItem item) {
        navDrawerItems.add(item);
    }

    public NavDrawerItem getNavDrawerItem(int i) {
        return navDrawerItems.get(i);
    }

    public void setNavDrawerItem(int i, NavDrawerItem item) {
        navDrawerItems.set(i, item);
    }

    public int getChildCount() {
        return navDrawerItems.size();
    }

    public String getTitle(){
        return this.title;
    }

    public int getIcon(){
        return this.icon;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setIcon(int icon){
        this.icon = icon;
    }
}
