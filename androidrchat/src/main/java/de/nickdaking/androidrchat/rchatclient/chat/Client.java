package de.nickdaking.androidrchat.rchatclient.chat;

import android.util.Base64;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

import de.nickdaking.androidrchat.rchatclient.fragments.FragmentChat;


/**
 * Created by Nickerchen on 13.02.14.
 */
public class Client extends Thread {

    private Socket socket;
    private BufferedReader input;
    private BufferedReader ndInput;
    private PrintWriter output;
    private Thread receiverThread;
    private FragmentChat fragmentChat;
    private Timer timer;
    private TimerTask task;
    private final long TIMEOUT = 2000; //10 sec

    public Client(FragmentChat fragmentChat) {
        this.fragmentChat = fragmentChat;
    }

    public void setFragment(FragmentChat fragmentChat) {
        this.fragmentChat = fragmentChat;
    }

    public void send(String message) {
        output.println(message);
        output.flush();
    }

    public void connect(String url, int port, String paircode) {
        receiverThread = new Thread(new ClientThread(url, port, paircode));
        timer = new Timer();
        task = new KeepAliveTask(this);
        timer.schedule(task, TIMEOUT, TIMEOUT);
        receiverThread.start();
    }

    public void disconnect() {
        try {
            send("disconnect");
            timer.cancel();
            task.cancel();
            receiverThread.interrupt();
            input.close();
            output.close();
            socket.close();
            socket = null;
        } catch (IOException ex) {

        }
    }

    public Boolean isConnected() {
        return socket != null && !socket.isClosed();
    }

    public void chat(String message) {
        message = message.replace("\n", "");
        this.send("chat:" + message);
    }

    class ClientThread implements Runnable {
        String url;
        String paircode;
        int port;

        ClientThread(String url, int port, String paircode) {
            this.paircode = paircode;
            this.url = url;
            this.port = port;
        }

        @Override
        public void run() {
            try {
                socket = new Socket(url, port);

                if (socket.isConnected()) {
                    output = new PrintWriter(new DataOutputStream(socket.getOutputStream()));
                    input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    send("connect:" + paircode);
                    send("getonlineplayers");
                } else {
                    return;
                }

                String buffer;
                while ((buffer = input.readLine()) != null) {
                    if (buffer.startsWith("msg")) {
                        fragmentChat.printHistoryln(new String(Base64.decode(buffer.split(":")[1], 0)));
                        continue;
                    }
                    if (buffer.startsWith("pong")) {
                        continue;
                    }
                    if (buffer.startsWith("onlineplayers")) {
                        String[] split = buffer.split(":");
                        StringBuilder playerList = new StringBuilder();
                        for (int i = 1; i < split.length; i++) {
                            playerList.append(split[i]);
                            if (!split[i].contains("#")) {
                                playerList.append(",");
                            }
                        }
                        if (playerList.length() > 0) {
                            playerList.deleteCharAt(playerList.length()-1);
                            fragmentChat.printHistoryln("<System> Players online: " + playerList.toString());
                        } else {
                            fragmentChat.printHistoryln("<System> No one is online.");
                        }
                        continue;
                    }
                    if (buffer.startsWith("BYE")) {
                        fragmentChat.printHistoryln("<System> Connection closed!");
                        return;
                    }
                    if (buffer.startsWith("+ok")) {
                        handleOk(buffer);
                        continue;
                    }
                    if (buffer.startsWith("+err")) {
                        handleErr(buffer);
                        continue;
                    }
                }
            } catch (UnknownHostException e1) {
                fragmentChat.printHistoryln("<System> Server offline!");
                e1.printStackTrace();
            } catch (IOException e1) {
                fragmentChat.printHistoryln("<System> Unknown error!");
                fragmentChat.printHistoryln("<System> " + e1.getMessage());
                e1.printStackTrace();
            }
        }

        public void handleErr(String err) {
            if (err.contains("{auth}")) {
                fragmentChat.printHistoryln("<System> Error! Wrong paircode. ");
                return;
            }
            if (err.contains("{protocol error}")) {
                fragmentChat.printHistoryln("<System> Protocol error!");
                return;
            }
            if (err.contains("{already connected}")) {
                fragmentChat.printHistoryln("<System> Already connected!");
                return;
            }
            if (err.contains("{too much messages}")) {
                fragmentChat.printHistoryln("<System> Don't spam. ");
                return;
            }
            if (err.contains("{chat is down}")) {
                fragmentChat.printHistoryln("<System> Chat has been disabled by admin. ");
                return;
            }
            if (err.contains("{you have been muted}")) {
                fragmentChat.printHistoryln("<System> You have been muted.");
                return;
            }

        }

        public void handleOk(String ok) {
            if (ok.contains("{auth}")) {
                fragmentChat.printHistoryln("<System> Connected to chat");
                return;
            }
            if (ok.contains("{chat}")) {
                fragmentChat.freeEdText();
                return;
            }
        }
    }
}
