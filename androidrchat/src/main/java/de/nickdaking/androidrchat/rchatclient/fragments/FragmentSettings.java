package de.nickdaking.androidrchat.rchatclient.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import de.nickdaking.androidrchat.rchatclient.R;
import de.nickdaking.androidrchat.rchatclient.MainActivity;

/**
 * Created by Nickerchen on 09.02.14.
 */
public class FragmentSettings extends Fragment {

    private EditText edPaircode;
    private EditText edHost;

    public static Fragment newInstance(Context context) {
        FragmentSettings f = new FragmentSettings();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View myInflatedView = inflater.inflate(R.layout.fragment_settings, container,false);

        edPaircode = (EditText) myInflatedView.findViewById(R.id.edPaircode);
        final SharedPreferences settings = getActivity().getSharedPreferences(MainActivity.PREFS_NAME, 0);
        edPaircode.setText(settings.getString("paircode", ""));
        edPaircode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String paircode = edPaircode.getText().toString();
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("paircode", paircode);
                    editor.commit();
                    return true;
                }
                return false;
            }
        });

        edHost = (EditText) myInflatedView.findViewById(R.id.edHost);
        edHost.setText(settings.getString("host", ""));
        edHost.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String host = edHost.getText().toString();
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("host", host);
                    editor.commit();
                    return true;
                }
                return false;
            }
        });

        return (ViewGroup) myInflatedView;
    }

}
