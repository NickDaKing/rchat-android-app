package de.nickdaking.androidrchat.rchatclient;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;

import de.nickdaking.androidrchat.rchatclient.adapter.NavDrawerListAdapter;
import de.nickdaking.androidrchat.rchatclient.fragments.FragmentChat;
import de.nickdaking.androidrchat.rchatclient.fragments.FragmentSettings;
import de.nickdaking.androidrchat.rchatclient.model.NavDrawerGroup;
import de.nickdaking.androidrchat.rchatclient.model.NavDrawerItem;

public class MainActivity extends Activity {
    private DrawerLayout mDrawerLayout;
    private ExpandableListView mDrawerList;
    private ExpandableListAdapter mDrawerlistAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
 
    // nav drawer title
    private CharSequence mDrawerTitle;
 
    // used to store app title
    private CharSequence mTitle;
 
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private FragmentChat fragmentChat;
    private ArrayList<NavDrawerGroup> navDrawerGroups;
    private static NavDrawerListAdapter adapter;

	public static final String PREFS_NAME = "BiberCraftPrefs";
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
 
        mTitle = mDrawerTitle = getTitle();
        fragmentChat = new FragmentChat();
 
        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_groups_titles);
 
        // nav drawer icons from resources
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_group_icons);
 
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ExpandableListView) findViewById(R.id.list_slidermenu);

        navDrawerGroups = new ArrayList<NavDrawerGroup>();


        // adding nav drawer items to array
        // Chat
        navDrawerGroups.add(new NavDrawerGroup(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1), 0, false, 0)));
        // Settings
        navDrawerGroups.add(new NavDrawerGroup(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1), 1)));

        // Recycle the typed array
        navMenuIcons.recycle();
 
        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerGroups);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setItemChecked(0, true);
        mDrawerList.setSelection(0);
 
        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
 
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ){
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }
 
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // Listview Group expanded listener
        mDrawerList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                mDrawerList.setItemChecked(groupPosition, true);
                mDrawerList.setSelection(groupPosition);
                if (navDrawerGroups.get(groupPosition).getChildCount() == 1) {
                    displayView(navDrawerGroups.get(groupPosition).getNavDrawerItem(0));
                }
            }
        });

		mDrawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener(){

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                mDrawerList.collapseGroup(groupPosition);
                displayView(navDrawerGroups.get(groupPosition).getNavDrawerItem(childPosition));
                return true;
            }
        });
 
        if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(navDrawerGroups.get(0).getNavDrawerItem(0));
        }
    }
	
	/**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(NavDrawerItem drawerItem) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (drawerItem.getGlobalPosition()) {
            case 0:
                fragment = fragmentChat;
                if (fragmentChat.isPaused()) {
                    fragment.setInitialSavedState(FragmentChat.FRAGMENTSTATE);
                }
                break;
            case 1:
                fragment = new FragmentSettings();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
            // update selected item and title, then close the drawer
            setTitle(drawerItem.getTitle());
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }
 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_about:
                Toast.makeText(this, "www.bibercraft.de", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_connection:
                displayView(navDrawerGroups.get(0).getNavDrawerItem(0));
                if (!fragmentChat.getClient().isConnected()) {
                    String paircode = "";
                    String url = "";
                    int port = 20070;
                    SharedPreferences settings = this.getSharedPreferences(PREFS_NAME, 0);
                    if (settings != null) {
                        paircode = settings.getString("paircode", "0");
                        String[] host = settings.getString("host", "webdev.local").split(":");
                        url = host[0];
                        if (host.length == 2) {
                            port = Integer.valueOf(host[1]);
                        }
                    }
                    fragmentChat.getClient().connect(url, port, paircode);
                    String msg = "<System> Connecting..."; //Outsource to Strings.xml!
                    fragmentChat.printHistoryln(msg);
                } else {
                    fragmentChat.getClient().disconnect();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
 
    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        return super.onPrepareOptionsMenu(menu);
    }
 
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }
 
    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
 
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
 
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public static NavDrawerListAdapter getNavDrawerAdpter() {
        return adapter;
    }
}