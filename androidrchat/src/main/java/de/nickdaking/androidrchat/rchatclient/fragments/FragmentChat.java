package de.nickdaking.androidrchat.rchatclient.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import de.nickdaking.androidrchat.rchatclient.MainActivity;
import de.nickdaking.androidrchat.rchatclient.R;
import de.nickdaking.androidrchat.rchatclient.chat.Client;
import de.nickdaking.androidrchat.rchatclient.model.NavDrawerItem;

public class FragmentChat extends Fragment {

    private TextView chatHistory;
    private ScrollView scrollView;
    private static final StringBuilder historyContent = new StringBuilder();
    private Button btSend;
    private Button btWho;
    private EditText edText;
    private View myInflatedView;
    private static Client CLIENT;
    public Activity activity;
    private Boolean paused = true;
    public static Fragment.SavedState FRAGMENTSTATE;
    private ListView listView;

    public FragmentChat() {
        if (CLIENT == null) {
            CLIENT = new Client(this);
        } else {
            CLIENT.setFragment(this);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listView = (ListView) getActivity().findViewById(R.id.list_slidermenu);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        paused = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        myInflatedView = inflater.inflate(R.layout.fragment_chat, container, false);

        chatHistory = (TextView) myInflatedView.findViewById(R.id.chatHistory);
        chatHistory.setTextColor(Color.parseColor("#000000"));
        scrollView = (ScrollView) myInflatedView.findViewById(R.id.chatScroll);

        if (savedInstanceState != null) {
            chatHistory.setText(Html.fromHtml(historyContent.toString()));
        }

        edText = (EditText) myInflatedView.findViewById(R.id.edMessage);
        edText.setImeActionLabel("Send", EditorInfo.IME_ACTION_DONE);
        edText.setMovementMethod(new ScrollingMovementMethod());
        edText.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (CLIENT.isConnected()) {
                                String text = edText.getText().toString();
                                CLIENT.chat(Base64.encodeToString(text.getBytes(), 0, text.getBytes().length, 0));
                            } else {
                                Toast.makeText(getActivity(), "Nicht verbunden!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        return false; // pass on to other listeners.
                    }
                });

        btSend = (Button) myInflatedView.findViewById(R.id.btSend);
        btSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CLIENT.isConnected()) {
                    String text = edText.getText().toString();
                    if (!text.isEmpty()) {
                        CLIENT.chat(Base64.encodeToString(text.getBytes(), 0, text.getBytes().length, 0));
                    }
                } else {
                    Toast.makeText(getActivity(), "Not connected!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btWho = (Button) myInflatedView.findViewById(R.id.btWho);
        btWho.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CLIENT.isConnected()) {
                    CLIENT.send("getonlineplayers");
                } else {
                    Toast.makeText(getActivity(), "Not connected!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return (ViewGroup) myInflatedView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("chatHistory", historyContent.toString());
    }

    @Override
    public void onPause() {
        super.onPause();
        paused = true;
        setCounterVisible(true);
        FRAGMENTSTATE = getFragmentManager().saveFragmentInstanceState(this);
    }

    public void onResume() {
        super.onResume();
        paused = false;
        setCounterVisible(false);

        chatHistory.setText(Html.fromHtml(historyContent.toString()));
    }

    public Boolean isPaused() {
        return paused;
    }

    public void printHistoryln(final String message) {
        String prefix = "";
        String prefixCleaned = "";
        if (message.lastIndexOf("<") != -1 && message.lastIndexOf(">") != -1) {
            prefix = message.substring(message.lastIndexOf("<"), message.lastIndexOf(">") + 1);
            prefixCleaned = message.substring(message.lastIndexOf(">") + 1, message.length());
        } else {
            prefix = message;
        }

        historyContent.append("<b>" + TextUtils.htmlEncode(prefix) + "</b>" + TextUtils.htmlEncode(prefixCleaned) + "<br>");

        if (isPaused()) {
            addCounter();
        } else {
            if (scrollView != null)
                scrollView.fullScroll(View.FOCUS_DOWN);
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    chatHistory.setText(Html.fromHtml(historyContent.toString()));
                    }
            });
        }
    }

    public void freeEdText() {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                edText.setText("");
            }
        });
    }

    private void setCounterVisible(Boolean visible) {
        NavDrawerItem navDrawerItem = (NavDrawerItem) MainActivity.getNavDrawerAdpter().getChild(0,0);
        if (!visible) {
            navDrawerItem.setCount(0);
        }
        navDrawerItem.setCounterVisibility(visible);
        activity.runOnUiThread(new Runnable() {
            public void run() {
                MainActivity.getNavDrawerAdpter().notifyDataSetChanged();
            }
        });
    }

    public void addCounter() {
        NavDrawerItem navDrawerItem = (NavDrawerItem) MainActivity.getNavDrawerAdpter().getChild(0,0);
        navDrawerItem.setCount(navDrawerItem.getCount() + 1);

        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    MainActivity.getNavDrawerAdpter().notifyDataSetChanged();
                }
            });
        }
    }

    public static Client getClient() {
        return CLIENT;
    }
}